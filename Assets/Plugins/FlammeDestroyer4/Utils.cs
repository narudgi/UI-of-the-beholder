using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Ewan Argouse Utility

public static class Vector3Extansions
{
	public static Vector3 WithX(this Vector3 v, float x)
	{
		return new Vector3(x, v.y, v.z);
	}

	public static Vector3 WithY(this Vector3 v, float y)
	{
		return new Vector3(v.x, y, v.z);
	}

	public static Vector3 WithZ(this Vector3 v, float z)
	{
		return new Vector3(v.x, v.y, z);
	}

	public static Vector3 WithZY(this Vector3 v, float x, float y)
	{
		return new Vector3(x, y, v.z);
	}

	public static Vector3 WithXZ(this Vector3 v, float x, float z)
	{
		return new Vector3(x, v.y, z);
	}
}

public static class ShuffleListExtensions
{

	/// <summary>
	/// Shuffle the list in place using the Fisher-Yates method.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <param name="list"></param>
	public static void Shuffle<T>(this IList<T> list)
	{
		System.Random rng = new System.Random();
		int n = list.Count;
		while (n > 1)
		{
			n--;
			int k = rng.Next(n + 1);
			T value = list[k];
			list[k] = list[n];
			list[n] = value;
		}
	}

	/// <summary>
	/// Return a random item from the list.
	/// Sampling with replacement.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <param name="list"></param>
	/// <returns></returns>
	public static T RandomItem<T>(this IList<T> list)
	{
		if (list.Count == 0) throw new System.IndexOutOfRangeException("Cannot select a random item from an empty list");
		return list[UnityEngine.Random.Range(0, list.Count)];
	}

	/// <summary>
	/// Removes a random item from the list, returning that item.
	/// Sampling without replacement.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <param name="list"></param>
	/// <returns></returns>
	public static T RemoveRandom<T>(this IList<T> list)
	{
		if (list.Count == 0) throw new System.IndexOutOfRangeException("Cannot remove a random item from an empty list");
		int index = UnityEngine.Random.Range(0, list.Count);
		T item = list[index];
		list.RemoveAt(index);
		return item;
	}
}

public static class TransformExtensions
{
	/// <summary>
	/// Makes the given game objects children of the transform.
	/// </summary>
	/// <param name="transform">Parent transform.</param>
	/// <param name="children">Game objects to make children.</param>
	public static void AddChildren(this Transform transform, GameObject[] children)
	{
		Array.ForEach(children, child => child.transform.parent = transform);
	}

	/// <summary>
	/// Makes the game objects of given components children of the transform.
	/// </summary>
	/// <param name="transform">Parent transform.</param>
	/// <param name="children">Components of game objects to make children.</param>
	public static void AddChildren(this Transform transform, Component[] children)
	{
		Array.ForEach(children, child => child.transform.parent = transform);
	}

	/// <summary>
	/// Sets the position of a transform's children to zero.
	/// </summary>
	/// <param name="transform">Parent transform.</param>
	/// <param name="recursive">Also reset ancestor positions?</param>
	public static void ResetChildPositions(this Transform transform, bool recursive = false)
	{
		foreach (Transform child in transform)
		{
			child.position = Vector3.zero;

			if (recursive)
			{
				child.ResetChildPositions(recursive);
			}
		}
	}

	/// <summary>
	/// Sets the layer of the transform's children.
	/// </summary>
	/// <param name="transform">Parent transform.</param>
	/// <param name="layerName">Name of layer.</param>
	/// <param name="recursive">Also set ancestor layers?</param>
	public static void SetChildLayers(this Transform transform, string layerName, bool recursive = false)
	{
		var layer = LayerMask.NameToLayer(layerName);
		SetChildLayersHelper(transform, layer, recursive);
	}

	static void SetChildLayersHelper(Transform transform, int layer, bool recursive)
	{
		foreach (Transform child in transform)
		{
			child.gameObject.layer = layer;

			if (recursive)
			{
				SetChildLayersHelper(child, layer, recursive);
			}
		}
	}

	/// <summary>
	/// Sets the x component of the transform's local position.
	/// </summary>
	/// <param name="x">Value of x.</param>
	public static void SetLocalX(this Transform transform, float x)
	{
		transform.localPosition = new Vector3(x, transform.localPosition.y, transform.localPosition.z);
	}

	/// <summary>
	/// Sets the y component of the transform's local position.
	/// </summary>
	/// <param name="y">Value of y.</param>
	public static void SetLocalY(this Transform transform, float y)
	{
		transform.localPosition = new Vector3(transform.localPosition.x, y, transform.localPosition.z);
	}

	/// <summary>
	/// Sets the z component of the transform's local position.
	/// </summary>
	/// <param name="z">Value of z.</param>
	public static void SetLocalZ(this Transform transform, float z)
	{
		transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, z);
	}

	/// <summary>
	/// Sets the x component of the transform's position.
	/// </summary>
	/// <param name="x">Value of x.</param>
	public static void SetX(this Transform transform, float x)
	{
		transform.position = new Vector3(x, transform.position.y, transform.position.z);
	}

	/// <summary>
	/// Sets the y component of the transform's position.
	/// </summary>
	/// <param name="y">Value of y.</param>
	public static void SetY(this Transform transform, float y)
	{
		transform.position = new Vector3(transform.position.x, y, transform.position.z);
	}

	/// <summary>
	/// Sets the z component of the transform's position.
	/// </summary>
	/// <param name="z">Value of z.</param>
	public static void SetZ(this Transform transform, float z)
	{
		transform.position = new Vector3(transform.position.x, transform.position.y, z);
	}

	public static void SetRotationX(this Transform transform, float x)
	{
		transform.localEulerAngles = new Vector3(x, transform.localEulerAngles.y, transform.localEulerAngles.z);
	}

	public static void SetRotationY(this Transform transform, float y)
	{
		transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, y, transform.localEulerAngles.z);
	}

	public static void SetRotationZ(this Transform transform, float z)
	{
		transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, z);
	}
	
	// The snapping code
	public static Vector3Int RoundPosition(this Transform transform, float snapValue)
	{
		if (snapValue == 0) return Vector3Int.down;
		transform.position = new Vector3
		(
			snapValue * Mathf.Round(transform.position.x / snapValue),
			snapValue * Mathf.Round(transform.position.y / snapValue),
			snapValue * Mathf.Round(transform.position.z / snapValue)
		);
		return new Vector3Int
		(
			Mathf.FloorToInt(transform.position.x / snapValue),
			Mathf.FloorToInt(transform.position.y / snapValue),
			Mathf.FloorToInt(transform.position.z / snapValue)
		);
	}

	public static void RoundLocalRotation(this Transform transform, float snapValue)
	{
		if (snapValue == 0) return;
		transform.localEulerAngles = new Vector3
		(
			snapValue * Mathf.Round(transform.localEulerAngles.x / snapValue),
			snapValue * Mathf.Round(transform.localEulerAngles.y / snapValue),
			snapValue * Mathf.Round(transform.localEulerAngles.z / snapValue)
		);
	}

	// The snapping code
	public static void RoundLocalScale(this Transform transform, float snapValue)
	{
		if (snapValue == 0) return;
		transform.localScale = new Vector3
		(
			snapValue * Mathf.Round(transform.localScale.x / snapValue),
			snapValue * Mathf.Round(transform.localScale.y / snapValue),
			snapValue * Mathf.Round(transform.localScale.z / snapValue)
		);
	}
}

/// <summary>
/// Extension methods for UnityEngine.GameObject.
/// </summary>
public static class GameObjectExtensions
{
	/// <summary>
	/// Gets a component attached to the given game object.
	/// If one isn't found, a new one is attached and returned.
	/// </summary>
	/// <param name="gameObject">Game object.</param>
	/// <returns>Previously or newly attached component.</returns>
	public static T GetOrAddComponent<T>(this GameObject gameObject) where T : Component
	{
		return gameObject.GetComponent<T>() ?? gameObject.AddComponent<T>();
	}

	/// <summary>
	/// Checks whether a game object has a component of type T attached.
	/// </summary>
	/// <param name="gameObject">Game object.</param>
	/// <returns>True when component is attached.</returns>
	public static bool HasComponent<T>(this GameObject gameObject) where T : Component
	{
		return gameObject.GetComponent<T>() != null;
	}
}