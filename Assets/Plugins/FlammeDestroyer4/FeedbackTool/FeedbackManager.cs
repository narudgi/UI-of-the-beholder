using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class FeedbackManager : MonoBehaviour
{
	public static FeedbackManager instance = default;
	public CinemachineVirtualCamera virtualCamera = default;
	public CinemachineStoryboard storyboard = default;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else
		{
			Destroy(gameObject);
			return;
		}
		Check();
	}

	private void Check()
	{
		Debug.Assert(virtualCamera != default, "Virtual Camera Reference missing on Feedback Manager");
		Debug.Assert(storyboard != default, "Storyboard Reference missing on Feedback Manager");
	}

	public void Wake()
	{
		this.enabled = true;
	}

	public void Flash(Color color, float alpha, float duration)
	{
		if (storyboard == default) return;
		WaitForSecondsRealtime waitForSeconds = new WaitForSecondsRealtime(duration);
		StartCoroutine(FlashCoroutine(color, alpha, waitForSeconds));
	}

	public void Shake(SignalSourceAsset signalSource, float attack, float sustainTime, float decay)
	{
		if (storyboard == default) return;
		CinemachineImpulseSource source = this.gameObject.AddComponent<CinemachineImpulseSource>();
		source.m_ImpulseDefinition.m_RawSignal = signalSource;
		source.m_ImpulseDefinition.m_TimeEnvelope.m_AttackTime = attack;
		source.m_ImpulseDefinition.m_TimeEnvelope.m_SustainTime = sustainTime;
		source.m_ImpulseDefinition.m_TimeEnvelope.m_DecayTime = decay;
		source.GenerateImpulse();
		Destroy(source);
		Stop();
	}

	public void Zoom(float fov, float duration, float transitionDuration)
	{
		if (virtualCamera == default) return;
		WaitForSecondsRealtime waitForSeconds = new WaitForSecondsRealtime(duration);
		StartCoroutine(ZoomCoroutine(fov, transitionDuration, waitForSeconds));
	}

	public void SlowTime(float time, float seconds)
	{
		WaitForSecondsRealtime waitForSeconds = new WaitForSecondsRealtime(seconds);
		StartCoroutine(SlowTimeCoroutine(time, waitForSeconds));
	}

	public void Delete(AudioSource audioSource)
	{
		WaitForSecondsRealtime waitForSeconds = new WaitForSecondsRealtime(audioSource.clip.length);
		StartCoroutine(DeleteCoroutine(audioSource.gameObject, waitForSeconds));
	}

	IEnumerator FlashCoroutine(Color color, float alpha, WaitForSecondsRealtime seconds)
	{
		storyboard.m_ShowImage = true;
		storyboard.m_Alpha = alpha;
		storyboard.m_Aspect = CinemachineStoryboard.FillStrategy.CropImageToFit;
		Texture2D texture = new Texture2D(2, 2, TextureFormat.ARGB32, false);
		texture.SetPixel(0, 0, color);
		texture.SetPixel(0, 1, color);
		texture.SetPixel(1, 0, color);
		texture.SetPixel(1, 1, color);
		texture.Apply();
		storyboard.m_Image = texture;
		yield return seconds;
		storyboard.m_Image = default;
		Destroy(texture);
		storyboard.m_ShowImage = false;

		Stop();
		yield break;
	}

	IEnumerator SlowTimeCoroutine(float time, WaitForSecondsRealtime seconds)
	{
		Time.timeScale = time;
		yield return seconds;
		Time.timeScale = 1;

		Stop();
		yield break;
	}

	IEnumerator DeleteCoroutine(GameObject gameObject, WaitForSecondsRealtime seconds)
	{
		yield return seconds;
		Destroy(gameObject);

		Stop();
		yield break;
	}

	IEnumerator ZoomCoroutine(float fov, float transitionDuration, WaitForSecondsRealtime seconds)
	{
		float baseFov = virtualCamera.m_Lens.FieldOfView;

		float i = 0;
		while (i < 1)
		{
			virtualCamera.m_Lens.FieldOfView = Mathf.Lerp(baseFov, fov, i);
			i += Time.deltaTime / transitionDuration;
			yield return null;
		}
		yield return seconds;
		i = 0;
		while (i < 1)
		{
			virtualCamera.m_Lens.FieldOfView = Mathf.Lerp(fov, baseFov, i);
			i += Time.deltaTime / transitionDuration;
			yield return null;
		}
		Stop();
		yield break;
	}

	private void Stop()
	{
		this.enabled = false;
	}
}
