using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

// FEEDBACK TOOL V1.1

[CreateAssetMenu(fileName = "New Feedback", menuName = "Feedback")]
public class Feedback : ScriptableObject
{
	//[Header("Animation")]
	//[SerializeField] private 
	[Header("Feedback Tool v1.1 -lazy-")]
	[Space]
	[Header("Particles")]
	[SerializeField] private ParticleSystem[] particles = default;
	[SerializeField] private ParticleSystem returnParticle = default;

	[Header("Sounds")]
	[SerializeField] private AudioClip sfx = default;

	[Header("Timescale Modification")]
	[SerializeField] private bool modifyTimescale = false;

	[SerializeField, ConditionalField("modifyTimescale")] private float time = .2f;
	[SerializeField, ConditionalField("modifyTimescale")] private float seconds = .1f;

	[Header("Camera Zoom")]
	[SerializeField] private bool zoomCamera = false;

	[SerializeField, ConditionalField("zoomCamera")] private float zoomFieldOfView = .2f;
	[SerializeField, ConditionalField("zoomCamera")] private float zoomTransitionDuration = .1f;
	[SerializeField, ConditionalField("zoomCamera")] private float zoomDuration = .1f;

	[Header("Camera Shake")]
	[SerializeField] private bool shakeCamera = false;

	[SerializeField, ConditionalField("shakeCamera")] private SignalSourceAsset signalSource = default;
	[SerializeField, ConditionalField("shakeCamera")] private float shakeAttack = 0f;
	[SerializeField, ConditionalField("shakeCamera")] private float shakeSustainTime = .2f;
	[SerializeField, ConditionalField("shakeCamera")] private float shakeDecay = .7f;

	[Header("Flash")]
	[SerializeField] private bool triggerFlash = false;

	[SerializeField, ConditionalField("triggerFlash")] private Color flashColor = new Color();
	[SerializeField, ConditionalField("triggerFlash")] private float flashDuration = 1;
	[SerializeField, ConditionalField("triggerFlash")] private float flashAlpha = .2f;

	[Header("Vibration")]
	[SerializeField] private bool triggerMobileVibration = false;

	/// <summary>
	/// Juice the game
	/// </summary>
	/// <returns>Return Particules</returns>
	public GameObject Juice(GameObject gameObject)
	{
		FeedbackManager manager = FeedbackManager.instance;
		if (particles != null || particles.Length > 0)
		{
			for (int i = 0; i < this.particles.Length; i++)
			{
				if (particles[i] == null) return null;
				GameObject particle = GameObject.Instantiate(this.particles[i].gameObject, gameObject.transform);
				particle.transform.position = gameObject.transform.position;
			}
		}
		if (returnParticle != null)
		{
			GameObject particle = GameObject.Instantiate(this.returnParticle.gameObject, gameObject.transform);
			particle.transform.position = gameObject.transform.position;
			return particle;
		}
		if (sfx != default)
		{
			manager.Wake();
			GameObject audioGameObject = new GameObject("temp - " + sfx.name);
			AudioSource tempAudio = audioGameObject.AddComponent<AudioSource>();
			tempAudio.clip = sfx;
			tempAudio.Play();
			manager.Delete(tempAudio);
		}
		if(modifyTimescale)
		{
			manager.SlowTime(time, seconds);
		}
		if (zoomCamera)
		{
			manager.Zoom(zoomFieldOfView, zoomDuration, zoomTransitionDuration);
		}
		if (shakeCamera)
		{
			manager.Shake(signalSource, shakeAttack, shakeSustainTime, shakeDecay);
		}
		if (triggerFlash)
		{
			manager.Flash(flashColor, flashAlpha, flashDuration);
		}
		if (triggerMobileVibration)
		{
			//Handheld.Vibrate();
		}
		return null;
	}
}