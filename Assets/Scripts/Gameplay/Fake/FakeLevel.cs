using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Level
{
	public float initial = 1;
	public int index = 1;
	public float Previous { get; private set; } = 1;
	private float current = 1;
	public float Current
	{
		get { return current; }
		set
		{
			current = value;

			Up(current);
		}
	}

	private void Up(float value)
	{
		if (value - Previous >= 1)
		{
			Previous = Mathf.FloorToInt(current);
			UpEvent(index);
		}
	}

	public Action<int> UpEvent = delegate { };
}

public class FakeLevel : MonoBehaviour
{
	[SerializeField] private Level[] level = new Level[4];
	public Action<int> LevelUpUI = delegate { };

	private void Start()
	{
		for (int i = 0; i < level.Length; i++)
		{
			level[i].UpEvent = LevelUp;
		}
	}

	[ContextMenu("Generate Xp")]
	public void Generate()
	{
		for (int i = 0; i < level.Length; i++)
		{
			float random = UnityEngine.Random.Range(.05f, 1.15f);

			level[i].Current += random;
		}
	}

	public float GetRatio(int index)
	{
		return  level[index].Current - level[index].Previous;
	}

	public float GetLevel(int index)
	{
		return level[index].Previous;
	}

	private void LevelUp(int index)
	{
		Debug.Log("- Polish here - Level Up");

		LevelUpUI(index);
        AudioManager.instance.PlayRandomSfx("LevelUp");
    }
}
