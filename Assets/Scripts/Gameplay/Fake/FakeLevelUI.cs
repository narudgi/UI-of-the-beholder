using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FakeLevelUI : MonoBehaviour
{
	[SerializeField] private int index = 0;
	[SerializeField] private float lerpSpeed = 1f;
	[SerializeField] private Image image = default;
	[SerializeField] private TextMeshProUGUI level = default;
	[SerializeField] private GameObject levelUpImage = default;
	private FakeLevel fakeLevel = default;

	private bool isLeveling = false;

	private void Start()
	{
        fakeLevel = FindObjectOfType<FakeLevel>();
		fakeLevel.LevelUpUI += LevelUp;
	}

	void Update()
    {
		if (isLeveling) return;
		if (image.fillAmount != fakeLevel.GetRatio(index))
		{
			image.fillAmount = Mathf.Lerp(image.fillAmount, fakeLevel.GetRatio(index), Time.deltaTime * lerpSpeed);
		}
		level.SetText(fakeLevel.GetLevel(index).ToString());
    }

	void LevelUp(int i)
	{
		if (i != index) return;
		StartCoroutine(LevelUpCoroutine());
	}

	IEnumerator LevelUpCoroutine()
	{
		isLeveling = true;

		float i = 0;
		while(i < 1)
		{
			image.fillAmount = Mathf.Lerp(image.fillAmount, 1, i);
			i += Time.deltaTime * lerpSpeed * 2;
			yield return null;
		}
		image.fillAmount = 0;
		isLeveling = false;
		yield break;
	}
	
}
