using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class progAbility : MonoBehaviour
{
    string rawCommand;
    InputField rawField;
    public string oldCommand;
    // Start is called before the first frame update
    void Start()
    {
        rawField = GameObject.Find("InputField").GetComponent<InputField>();
    }

    // Update is called once per frame
    void Update()
    {
        rawField.Select();
        if (Input.GetKeyDown("return"))
        {
            EnterTheconsole();
			FindObjectOfType<InputAlwaysOn>().Start();
        }

    }

    void EnterTheconsole()
    {
        rawCommand = rawField.text;

        Text old1field = GameObject.Find("Old1").GetComponent<Text>();
        Text old2field = GameObject.Find("Old2").GetComponent<Text>();
        Text old3field = GameObject.Find("Old3").GetComponent<Text>();
        Text old4field = GameObject.Find("Old4").GetComponent<Text>();
        Text old5field = GameObject.Find("Old5").GetComponent<Text>();
        Text old6field = GameObject.Find("Old6").GetComponent<Text>();
        Text old7field = GameObject.Find("Old7").GetComponent<Text>();
        Text old8field = GameObject.Find("Old8").GetComponent<Text>();

        rawCommand = rawCommand + ' ';
        rawField.text = "";
        old8field.text = old7field.text;
        old7field.text = old6field.text;
        old6field.text = old5field.text;
        old5field.text = old4field.text;
        old4field.text = old3field.text;
        old3field.text = old2field.text;
        old2field.text = old1field.text;
        old1field.text = rawCommand;
        string currentWord = "";
        //string validation
        //foreach (var here in rawCommand)
        rawField.Select();
        bool error;
        error = true;
        foreach (char c in rawCommand)
        {
            if (c == ' ')
            {
                if (currentWord == "clear")
                {
					currentWord = "";
					rawCommand = "";
					error = false;
					GoodCommand();
					GameManager.instance.ClearInk();

				}
				else if (currentWord == "reset")
                {
					currentWord = "";
					rawCommand = "";
					error = false;
					GoodCommand();
					GameManager.instance.ResetInterface();
				}
				else if (currentWord == "instantiate")
                {
					currentWord = "";
					rawCommand = "";
					error = false;
					GoodCommand();
					Instantiate(GameManager.instance.RandomPrefab(), transform.parent.parent);
				}
				else if ((currentWord == "help"))
                {
                    old8field.text = " ";
                    old7field.text = " ";
                    old6field.text = " ";
                    old4field.text = "=== Liste des commandes ===";
                    old3field.text = "clear - Clear screen";
                    old2field.text = "reset - Defaut interface";
                    old1field.text = "instantiate - restore an interface";
                    error = false;
                }
            }
            else
            {
                currentWord = currentWord + c.ToString();
            }
        }

        if (error == true)
        {
            BadCommand();
        }
    }
    void GoodCommand()
    {
        AudioManager.instance.PlayRandomSfx("PROG_Sucess");
    }

    void BadCommand()
    {
        AudioManager.instance.PlayRandomSfx("PROG_Wrong");
    }

	// oof
}
