using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class PunchlineSpawn : MonoBehaviour
{

    [SerializeField] bool isFighting = true;
    [SerializeField] float minSpawnTime = 2f;
    [SerializeField] float maxSpawnTime = 10f;
    public GameObject PunchlinePrefab = default;
	public Transform SpawnPoint = default;
	GameObject punchline = default;
    bool isWaiting = false;

    // Update is called once per frame
    void Update()
    {
        if (isFighting && !isWaiting)
        {
			if (punchline != null) return;
            StartCoroutine (SpawnNewPunchline(Random.Range(minSpawnTime,maxSpawnTime)));
            isWaiting = true;
        }
    }

    IEnumerator SpawnNewPunchline(float timeToWait){
        Debug.Log("New punchline incoming");
		yield return new WaitForSeconds(timeToWait);
        punchline = Instantiate(PunchlinePrefab, SpawnPoint.position, Quaternion.identity);
		punchline.transform.SetParent(transform);
        punchline.transform.localPosition = Vector3.zero;
        punchline.transform.localScale = new Vector3(1f, 1f, 1f);
		ResetGameObject();
		isWaiting = false;
        AudioManager.instance.PlayRandomSfx("GD_Punchline");
		yield break;
	}

	public void ResetGameObject()
	{
		GetComponent<Dragndrop>().collider = punchline.GetComponentInChildren<Collider2D>();
		GetComponent<Dragndrop>().item = punchline;
	}

	public void RemoveRef()
	{
		GetComponent<Dragndrop>().collider = null;
		GetComponent<Dragndrop>().item = null;
	}

	//random coroutine quand combat
	//spawn punchline avec text aléatoire
	//text correspond a effet
	//drop applique l'effet (et lecture de la punchline ?)
}
