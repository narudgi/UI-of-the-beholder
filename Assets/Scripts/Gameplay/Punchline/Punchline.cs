using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public class PunchlineList
{
	[TextArea(2,3)]
    public string text;
    public float dammage;
}

public class Punchline : MonoBehaviour
{
    public List<PunchlineList> punchlineList = new List<PunchlineList>();
    public TextMeshProUGUI punchline;
    [SerializeField] float deleteTime = 4;

    // Start is called before the first frame update
    void Start()
    {
        SelectText();
        Despawn();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SelectText(){
        punchline.text = punchlineList[Random.Range(0, punchlineList.Count)].text;
    }

    void Despawn(){
        Destroy(transform.parent.gameObject, deleteTime);
    }

	private void OnDestroy()
	{
		FindObjectOfType<PunchlineSpawn>().RemoveRef();
	}
}
