using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryReceiver : MonoBehaviour
{
    public Image PlayerIcone1;
    public Image PlayerIcone2;
    public GameObject this[int index] => lifes[index];
    public int Count()
    {
        return lifes.Count;
    }
    [SerializeField] List<GameObject> lifes = default;
    [SerializeField] List<GameObject> lockedLifes = default;
	[SerializeField] int minimum = 2;

	[SerializeField] Color color = default;
    [SerializeField] Image imageColor = default;

    GameManager gameManager = default;

    private void Start()
    {
        gameManager = GameManager.instance;
    }

    private void Update()
    {
        if (lifes.Count <= 0)
        {
            Dead();
        }
    }

    private void OnMouseOver()
    {
        if (Input.GetMouseButtonUp(1))
        {
			if (!gameManager.dragObject) return;
            if(gameManager.dragObject.GetComponent<InterfaceState>().ownReceiver.Count() <= minimum)
			{
                PlayerDamaged();
                return;
            }
            Assign(gameManager.dragColorImage);
            AddLife(gameManager.dragObject);
        }
    }

    public Color GetColor()
    {
        return color;
    }

    public void Assign(Image image)
    {
        if (image == null) return;
		if (lockedLifes.Contains(gameManager.dragObject.gameObject)) return;
        image.color = color;
    }

    public void AddLife(Dragndrop dragndrop)
    {
        if (dragndrop == null) return;
        if (lifes.Contains(dragndrop.gameObject)) return;
        lifes.Add(dragndrop.gameObject);
    }

    public void Damage(Dragndrop dragndrop)
    {
        if (dragndrop == null) return;
        if (!lifes.Contains(dragndrop.gameObject)) return;
        lifes.Remove(dragndrop.gameObject);
        PlayerDamaged();
    }

    public void RemoveNotDamage(GameObject gameObject)
    {
        if (gameObject == null) return;
        if (!lifes.Contains(gameObject) || lifes.Count <= minimum) {
            PlayerDamaged();
            return;
        }
        lifes.Remove(gameObject);
    }

    void Dead()
    {
        Debug.Log(name + " mort, faut ajouter un feedback");
        Destroy(gameObject);
    }

    void PlayerDamaged() {
        PlayerIcone1.enabled = false;
        PlayerIcone2.enabled = true;
        StartCoroutine (PlayerUndamaged());
    }
    IEnumerator PlayerUndamaged(){
		yield return new WaitForSeconds(1);
        PlayerIcone1.enabled = true;
        PlayerIcone2.enabled = false;
		yield break;
	}

}
