﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CharacterCreation : MonoBehaviour
{
    InputField sdField;
    InputField gdField;
    InputField gaField;
    InputField progField;

    public string sdName;
    public string gdName;
    public string gaName;
    public string progName;

    public void ClickLeRetour()
    {
        sdField = GameObject.Find("SDName").GetComponent<InputField>();
        sdName = sdField.text;

        gdField = GameObject.Find("GDName").GetComponent<InputField>();
        gdName = gdField.text;

        gaField = GameObject.Find("GAName").GetComponent<InputField>();
        gaName = gaField.text;

        progField = GameObject.Find("ProgName").GetComponent<InputField>();
        progName = progField.text;

        if (sdName.Length < 1)
        {
            StartCoroutine(Wrongsd());
        }
        
        else if (gdName.Length < 1)
        {
            StartCoroutine(Wronggd());
        }

        else if (gaName.Length < 1)
        {
            StartCoroutine(Wrongga());
        }

        else if (progName.Length < 1)
        {
            StartCoroutine(Wrongprog());
        }

        else
        {
            SceneManager.LoadScene("THEGAME", LoadSceneMode.Single);
        }
    }

    IEnumerator Wronggd()
    {
        gdField.interactable = false;
        yield return new WaitForSeconds(1);
        gdField.interactable = true;
    }

    IEnumerator Wrongga()
    {
        gaField.interactable = false;
        yield return new WaitForSeconds(1);
        gaField.interactable = true;
    }

    IEnumerator Wrongprog()
    {
        progField.interactable = false;
        yield return new WaitForSeconds(1);
        progField.interactable = true;
    }

    IEnumerator Wrongsd()
    {
        sdField.interactable = false;
        yield return new WaitForSeconds(1);
        sdField.interactable = true;
    }
    private void Start()
    {
        
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
