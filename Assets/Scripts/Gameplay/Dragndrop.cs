using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(Rigidbody2D))]
public class Dragndrop : MonoBehaviour
{
    public enum Click
    {
        NONE,
        LEFT,
        RIGHT
    }

    Camera cam = default;
    PolygonCollider2D confiner2D = default;
	public new Collider2D collider = default;
    [SerializeField] Rigidbody2D rb = default;
    public GameObject item = default;

    [SerializeField] GameObject prefabItem3D = default;
    [SerializeField] Animator animator = default;
    [SerializeField] float throwForce = 10f;
    [SerializeField] float followSpeed = 2f;
	Vector2 mousePosition = default;
    Click click = default;
    bool drag = false;
    bool isFill;
    GameManager gameManager;
    public GameObject[] objectsArray;
    public GameObject[] itemsArray;
    bool isItem = false;

    private void OnValidate()
    {
        if (rb != default) return;
        rb = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
       
        gameManager = GameManager.instance;
        cam = GameObject.FindGameObjectWithTag("UICam").GetComponent<Camera>();
        confiner2D = GameObject.FindGameObjectWithTag("UIConfiner").GetComponent<PolygonCollider2D>();
    }

    private void Update()
    {
       
        if (FindObjectOfType<MouseController>().isEnemy == true && isItem == false)
        {
            itemsArray = GameObject.FindGameObjectsWithTag("Item");
            isItem = true;
        }

        else if (FindObjectOfType<MouseController>().isEnemy == false && isItem == true)
        {

            for (int i = 0; i < itemsArray.Length; i++)
            {

                if (itemsArray[i] != null )
                {
                    itemsArray[i].gameObject.SetActive(true);
                   
                }
            }
           
            isItem = false;
        }

        objectsArray = GameObject.FindGameObjectsWithTag("Object");
        if (objectsArray.Length == 0)
        {
            FindObjectOfType<GameManager>().GameOver();
        }
        mousePosition = cam.ScreenToWorldPoint(Input.mousePosition);

        if (drag)
        {
			if (item == null) return;
            if (click.Equals(Click.RIGHT))
            {
                rb.position = new Vector2
                (
                    Mathf.Clamp(mousePosition.x, confiner2D.bounds.center.x + .2f, -confiner2D.bounds.center.x),
                    Mathf.Clamp(mousePosition.y, 1.5f, confiner2D.bounds.center.y - .2f)
                );
            }
            else if(click.Equals(Click.LEFT))
            {
                item.transform.SetParent(transform.parent);
                item.transform.position = new Vector2
                (
                    Mathf.Clamp(mousePosition.x, confiner2D.bounds.center.x + .2f, -confiner2D.bounds.center.x),
                    Mathf.Clamp(mousePosition.y, 1.5f, confiner2D.bounds.center.y - .2f)
                );
            }
            
        }
        if (Input.GetMouseButtonUp(0) && drag)
        {
            click = Click.NONE;

			if (prefabItem3D == null) return;
			OnReleaseOut();
        }

        if (Input.GetMouseButtonUp(1) && drag)
        {
            click = Click.NONE;
            OnRelease();
        }
    }
    
    private void OnMouseOver()
    {
		if (rb.velocity.magnitude > 5f) return;
		animator.SetBool("Over", true);

		mousePosition = cam.ScreenToWorldPoint(Input.mousePosition);
		Vector2 vectorToTarget = mousePosition - (Vector2)transform.position;
		if (vectorToTarget.magnitude > .1f) transform.position += (Vector3) vectorToTarget.normalized * followSpeed * Time.deltaTime;

		if (Input.GetMouseButtonDown(0))
        {

			if (prefabItem3D == null) return;
			click = Click.LEFT;
            OnClick();
        }

        if (Input.GetMouseButtonDown(1))
        {
            click = Click.RIGHT;
            OnClick();
        }
    }

    void OnRelease()
    {
        Throw();
        InventoryReceiver[] receivers = FindObjectsOfType<InventoryReceiver>();
        foreach (InventoryReceiver receiver in receivers)
        {
            for (int i = 0; i < receiver.Count(); i++)
            {
                if (receiver[i].GetComponent<InterfaceState>().GetColor() != receiver.GetColor())
                {
                    receiver.RemoveNotDamage(receiver[i]);
                }
            }
        }
		Drawing drawing = GetComponent<Drawing>();
		if (drawing)
		{
			if (!drawing.erase)
			{
				if (FindObjectOfType<MouseController>().isEnemy == true)
				{
					FindObjectOfType<MouseController>().hitEnemyCollider.GetComponent<EnemyController>().Damage(1);
				}

				Instantiate(prefabItem3D);
			}
		}
        collider.enabled = true;
        gameManager.drag = false;
        drag = false;
    }

    void OnReleaseOut()
    {
        item.SetActive(false);
        item.transform.SetParent(transform);
        item.transform.localPosition = Vector3.zero;
		if(GameManager.instance.dragObject) FindObjectOfType<PunchlineSpawn>().ResetGameObject();
        collider.enabled = true;
        gameManager.drag = false;
        drag = false;
        //throw object

        if (FindObjectOfType<MouseController>().isEnemy == true)
        {
            FindObjectOfType<MouseController>().hitEnemyCollider.GetComponent<EnemyController>().Damage(1);
        }

        Instantiate(prefabItem3D);
    }

    void OnClick()
    {
        transform.SetAsLastSibling();
        collider.enabled = false;
        animator.SetBool("Click", true);
        drag = true;
        gameManager.drag = true;
        gameManager.dragObject = null;
        gameManager.dragColorImage = null;
        rb.velocity = Vector2.zero;
        AudioManager.instance.PlayRandomSfx("UI_Hold");
    }

    private void OnMouseExit()
    {
        animator.SetBool("Over", false);
        AudioManager.instance.PlayRandomSfx("UI_Release");
    }

    void Throw()
    {
        animator.SetBool("Click", false);

        Vector2 dir = mousePosition - (Vector2) transform.position;

        dir.Normalize();

        if(dir.magnitude > .5f) rb.velocity = dir * throwForce;
    }

    public bool isDrag()
    {
        return drag;
    }

    
}
