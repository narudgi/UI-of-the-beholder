using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drawing : MonoBehaviour
{
    public bool erase = false;
	[SerializeField] GameObject[] lines = default;

	int index = 0;

	private void Start()
	{
		for (int i = 0; i < lines.Length; i++)
		{
			Vector2 _dir = transform.position - lines[i].transform.position;
			float _length = _dir.magnitude;
			lines[i].transform.position = transform.position;
		}
		GameManager.instance.ClearInk += Clear;
	}

	private void Update()
	{
		if (erase || GameManager.instance.drag) return;

		Vector2 dir = transform.position - lines[index].transform.position;
		float length = dir.magnitude;
		if (GetComponent<Rigidbody2D>().velocity.magnitude < 2f)
		{
			return;
		}

		SetPoint();
	}

	void SetPoint()
	{
		lines[index].transform.position = transform.position;
		if (!lines[index].activeSelf) lines[index].SetActive(true);

		if (GetComponent<Rigidbody2D>().velocity.magnitude < 5f)
		{
			return;
		}

		index++;
		if (index > lines.Length - 1) index = 0;
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (erase)
		{
			collision.gameObject.SetActive(false);
		}
	}

	public void Clear()
	{
		for (int i = 0; i < lines.Length; i++)
		{
			Vector2 _dir = transform.position - lines[i].transform.position;
			float _length = _dir.magnitude;
			lines[i].transform.position = transform.position;
			lines[i].gameObject.SetActive(false);
		}
	}
}
