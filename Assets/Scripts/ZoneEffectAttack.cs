﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZoneEffectAttack : MonoBehaviour
{
    Color color;
    bool isBoom;
    float positionX;
    float positionY;
    // Start is called before the first frame update
    void Start()
    {
        positionX = Random.Range(-150, 375);
        positionY = Random.Range(-150, 150);
        GetComponent<RectTransform>().localPosition = new Vector3(positionX, positionY, 0);
        color = GetComponent<Image>().color;
        StartCoroutine(Boom());
    }

    // Update is called once per frame
    void Update()
    {
  
    }

    IEnumerator Boom()
    {
        yield return new WaitForSeconds(1.5f);
        
        GetComponent<CircleCollider2D>().enabled = true;
        color.a = 255;
        GetComponent<Image>().color = color;
        if (isBoom == false)
        {
            transform.position += new Vector3(0.1f, 0, 0);
            isBoom = true;
        }

        yield return new WaitForSeconds(1.5f);
        Destroy(gameObject);
        yield break;

    }

    private void OnTriggerEnter2D(Collider2D other)

    {
        
        if (other.gameObject.tag == "Object")
        {
            Destroy(other.gameObject);
        }
      
    }
}
