using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceState : MonoBehaviour
{
    [SerializeField] Image colorImage = default;
    public InventoryReceiver ownReceiver;
    Dragndrop dragndrop;

    private void Start()
    {
        dragndrop = GetComponent<Dragndrop>();
        InventoryReceiver[] receivers = FindObjectsOfType<InventoryReceiver>();
        foreach(InventoryReceiver receiver in receivers)
        {
            if (receiver.GetColor() == colorImage.color)
            {
                receiver.AddLife(dragndrop);
                ownReceiver = receiver;
            }
        }
    }

    private void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(1))
        {
            GameManager.instance.dragColorImage = colorImage;
            GameManager.instance.dragObject = dragndrop;
        }
    }

    public Color GetColor()
    {
        return colorImage.color;
    }
}
