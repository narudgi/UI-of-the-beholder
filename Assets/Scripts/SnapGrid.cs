using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SnapGrid: MonoBehaviour
{

	[SerializeField] public Vector3Int position;

	[Space]
	[Header("Parameters")]
	[SerializeField] private bool snapToGrid = true;
	[SerializeField] private float snapValue = 0.5f;


#if UNITY_EDITOR

	[SerializeField] private bool snapRotation = true;
	[SerializeField, ConditionalField("snapRotation")] private float snapRotationValue = 0.5f;

	[SerializeField] private bool sizeToGrid = false;
	[SerializeField, ConditionalField("sizeToGrid")] private float sizeValue = 0.25f;

	void Update()
	{
		SnapToGrid();
	}

	void SnapToGrid()
	{
		if (snapToGrid)
			position = transform.RoundPosition(snapValue);
		if (snapRotation)
			transform.RoundLocalRotation(snapRotationValue);
		if (sizeToGrid)
			transform.RoundLocalScale(sizeValue);
	}

#endif

	public float GetSnapValue()
	{
		return snapValue;
	}

	public (int X, int Y, int Z) GetPosition()
	{
		return (X: position.x, Y: position.y, Z: position.z);
	}
}
