﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseController : MonoBehaviour
{
    
    [SerializeField] private float lengthRay;
    [SerializeField] private float speedRotation;
    [SerializeField] private float timer;
    float timerOffset;
    private float smootSpeed;
    bool isFire;
    public GameObject focusRotation;
    SnapGrid snapGrid;
    RaycastHit hitEnemy;
    public bool isEnemy;
    public Collider hitEnemyCollider;
    bool isTimer;
    GameManager gameManager;
    public float timerMove;
    float timerOffset2;


    // Start is called before the first frame update
    void Start()
    {
        snapGrid = GetComponent<SnapGrid>();
        gameManager = GameManager.instance;
        smootSpeed = speedRotation;
    }

    // Update is called once per frame
    void Update()
    {
        DetectionLeft();
        DetectionRight();
        hitEnemyCollider = hitEnemy.collider;
        EnemyAutoRotate();
        Move();
        Rotate();

        Debug.DrawRay(transform.position, transform.forward, Color.yellow);
        Debug.DrawRay(transform.position, -transform.forward, Color.yellow);
        Debug.DrawRay(transform.position, transform.right, Color.yellow);
        Debug.DrawRay(transform.position, -transform.right, Color.yellow);
        
        if (snapGrid.GetPosition().X == 6 && snapGrid.GetPosition().Z == -12)
        {
            FindObjectOfType<GameManager>().GameOver();
        }

    }

    void Move()
    {
        if (Time.time >= timerOffset2)
        {
            if (gameManager.drag) return;
            if (DetectionForward() == false && isFire == false)
            {
                if (Input.GetAxis("Mouse ScrollWheel") > 0)
                {

                    transform.position += transform.forward * snapGrid.GetSnapValue();
                    timerOffset2 = Time.time + timerMove;
                }
            }

            if (DetectionBackward() == false && isFire == false && isEnemy == false)
            {
                if (Input.GetAxis("Mouse ScrollWheel") < 0)
                {
                    isTimer = true;
                    transform.position += transform.forward * -snapGrid.GetSnapValue();
                    timerOffset2 = Time.time + timerMove;
                }
            }
        }
      

    }


    void Rotate()
    {
        if (gameManager.drag) return;
        if (isEnemy == false)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, focusRotation.transform.rotation, speedRotation);
            speedRotation += Time.deltaTime;
            if (transform.rotation.y >= focusRotation.transform.rotation.y - 3)
            {
                isFire = false;
            }


            if (Input.GetButtonDown("Fire1"))
            {
                //transform.rotation =  Quaternion.Euler(0, transform.eulerAngles.y - 90,0);
                speedRotation = smootSpeed;
                focusRotation.transform.Rotate(new Vector3(0, -90, 0));
                isFire = true;
            }



            else if (Input.GetButtonDown("Fire2"))
            {
                //transform.rotation = Quaternion.Euler(0, transform.eulerAngles.y + 90, 0);


                speedRotation = smootSpeed;
                focusRotation.transform.Rotate(new Vector3(0, 90, 0));
                isFire = true;
            }


        }

    }


    void EnemyAutoRotate()
    {
        if (hitEnemy.collider != null && hitEnemy.collider.gameObject.tag == "Enemy")
        {
            isEnemy = true;
            transform.LookAt( hitEnemy.transform);
        }

        if (hitEnemy.collider == null)
        {
            isEnemy = false;
        }
    }


    bool DetectionForward()
    {
        RaycastHit hit;
        if(Physics.Raycast(transform.position, transform.forward, out hit, lengthRay))
        {
            if (hit.collider.tag == "Wall" || hit.collider.tag == "Enemy")
            {
                if (hit.collider.tag == "Enemy")
                {
                    hitEnemy = hit;
                }

                return true;
            }
        }

       

        return false;
    }

    bool DetectionBackward()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, -transform.forward , out hit, lengthRay))
        {
            if (hit.collider.tag == "Wall" || hit.collider.tag == "Enemy")
            {
                if (hit.collider.tag == "Enemy")
                {
                    hitEnemy = hit;
                }
                return true;
            }
        }



        return false;
    }

    bool DetectionRight()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.right, out hit, lengthRay))
        {
            if (hit.collider.tag == "Wall" || hit.collider.tag == "Enemy")
            {
                if (hit.collider.tag == "Enemy")
                {
                    hitEnemy = hit;
                }
                return true;
            }
        }



        return false;
    }

    bool DetectionLeft()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, -transform.right , out hit, lengthRay))
        {
            if (hit.collider.tag == "Wall" || hit.collider.tag == "Enemy")
            {
                if (hit.collider.tag == "Enemy")
                {
                    hitEnemy = hit;
                }
                return true;
            }
        }



        return false;
    }

    void ItemRepop()
    {
        
        if (isEnemy == false)
        {
            FindObjectOfType<Dragndrop>().item.gameObject.SetActive(true);
        }

       
    }
   




}
