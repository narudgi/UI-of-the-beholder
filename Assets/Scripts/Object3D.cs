using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Object3D : MonoBehaviour
    
{
    bool isFlying;
    MouseController mouseController;

    void Start()
    {
       
            transform.position = GameObject.FindGameObjectWithTag("Player").transform.position;
        
        mouseController = GameObject.FindGameObjectWithTag("Player").GetComponent<MouseController>();

		StartCoroutine(FlyToEnemy());
	}

    IEnumerator FlyToEnemy()
    {
		float i = 0;
        while(i < 1)
        {
            if(mouseController.isEnemy) transform.position = Vector3.MoveTowards(transform.position, mouseController.hitEnemyCollider.transform.position, i * 20);
            if(!mouseController.isEnemy) transform.position = Vector3.MoveTowards(transform.position, transform.forward, i * 10);
			i +=Time.deltaTime;
            yield return null;

        }
		yield break;
    }

    private void OnTriggerEnter(Collider other)
    {
        
    }
}
