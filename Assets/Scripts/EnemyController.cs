using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public GameObject moi;
    public float life = 2;
    RaycastHit hit;
    RaycastHit hit2;
    public float lengthHit;
    SnapGrid snapGrid;
    float timerMove;
    public float speedEnemy;
    public GameObject[] attacksArray;
    MouseController mouseController;
    public Canvas canvas;
     float timerAttack;
    public float timerAttackOffset;
    bool isAttacking;

    int randomAttack;
    // Start is called before the first frame update
    void Start()
    {
        snapGrid = GetComponent<SnapGrid>();
        mouseController = GameObject.FindGameObjectWithTag("Player").GetComponent<MouseController>();
    }

    // Update is called once per frame
    void Update()
    {
        Death();
        Attack();
        Move();
        if (life <= 0){
            Destroy(moi);
        }

        transform.LookAt(GameObject.FindGameObjectWithTag("Player").transform);
    }

    void Death()
    {
        if (life <= 0)
        {
           
            Destroy(gameObject);
			FindObjectOfType<FakeLevel>().Generate();
        }
    }

    public void Damage(float dammage)
    {
        life -= dammage;
        AudioManager.instance.PlayRandomSfx("Enemies_Hit");
    }

    void Move()
    {
        if (Detection() == true && hit.collider.GetComponent<MouseController>().isEnemy == false)
        {
            
            if (Time.time >= timerMove )
            {
                timerMove = 0;
                timerMove += Time.time + speedEnemy;
                if (transform.eulerAngles == (new Vector3(0, 180, 0)))
                {
                    transform.localPosition += new Vector3(0, 0, -snapGrid.GetSnapValue());
                }

                else if (transform.eulerAngles == (new Vector3(0, 90, 0)))
                {
                    transform.localPosition += new Vector3(snapGrid.GetSnapValue(), 0, 0);
                }

                else if (transform.eulerAngles == (new Vector3(0, 0, 0)) || transform.eulerAngles == (new Vector3(0, 360, 0))  )
                {
                    transform.localPosition += new Vector3(0, 0, snapGrid.GetSnapValue());
                }

                else if (transform.eulerAngles == (new Vector3(0, -90, 0)) || transform.eulerAngles == (new Vector3(0, 270, 0)))
                {
                    transform.localPosition += new Vector3(-snapGrid.GetSnapValue(), 0, 0);
                }



            }
             
        }
        
        Debug.DrawRay(transform.position, transform.forward * lengthHit, Color.red);
        Debug.DrawRay(transform.position, -transform.forward * lengthHit, Color.red);
    }

    bool Detection()
    {
        
        if(Physics.Raycast(transform.position, transform.forward, out hit, lengthHit ) )
        {
            if (hit.collider.tag == "Player")
            {
                return true;
                
            }

            
        }
        return false;
    }

    void Attack()
    {
       if (mouseController.isEnemy == true && mouseController.hitEnemyCollider != null && mouseController.hitEnemyCollider.name == this.gameObject.name)
        {
            if (isAttacking == false)
            {
                timerAttack = Random.Range(0, 3);
                timerAttackOffset = Time.time + timerAttack;
                isAttacking = true;
            }

            if (Time.time >= timerAttackOffset && isAttacking == true)
            {
                isAttacking = false;
                randomAttack = Random.Range(0, 2);
                Instantiate(attacksArray[randomAttack], canvas.transform);
                if (randomAttack == 0)
                {
                    Instantiate(attacksArray[randomAttack], canvas.transform);
                    Instantiate(attacksArray[randomAttack], canvas.transform);
                }
                timerAttackOffset = 0;
                
                

            }
            
        }
    }
}
