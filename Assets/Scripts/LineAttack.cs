﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LineAttack : MonoBehaviour
{
    public GameObject moi;
    float randomRotation;
    float randomPositionX;
    float randomPositionY;
    float randomSpeedX;
    float randomSpeedY;
    Color color;
    // Start is called before the first frame update
    void Start()
    {
        randomPositionX = Random.Range(-200, 200);
        randomPositionY = Random.Range(-150, 150);
        randomSpeedX = Random.Range(-.2f, .2f);
        randomSpeedY = Random.Range(-.2f, .2f);
        //GetComponent<RectTransform>().localPosition = new Vector3(randomPositionX, randomPositionY, 0);
        color = GetComponent<Image>().color;

        Destroy(gameObject, 5f);
        StartCoroutine(Move());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)

    {

        if (other.gameObject.tag == "Object")
        {
            Destroy(other.gameObject);
        }
        else if (other.gameObject.tag == "Shield"){
            Destroy(moi);
        }

    }

    IEnumerator Move()
    {
        yield return new WaitForSeconds(1.5f);
        GetComponent<CircleCollider2D>().enabled = true;
        color.a = 255;
        GetComponent<Image>().color = color;
        float i = 0;
        while (i < 5)
        {
            transform.position += new Vector3(randomSpeedX, randomSpeedY, 0);
            i += Time.deltaTime;
            yield return null;
        }
        yield break;
    }
    

}
