using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public bool drag = false;
    public Image dragColorImage = default;
    public Dragndrop dragObject = default;
    public GameObject[] prefabIcon = default;
	public Action ResetInterface = delegate { };
    public Action ClearInk = delegate { };
	AudioClip ac;
    float timer;
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
         
    }

    private void Start()
    {
        AudioManager.instance.PlayRandomSfx("Musics");
    }

    private void Update()
    {
        if (Time.time >= timer)
        {
            AudioManager.instance.PlayRandomSfx("Music");
            timer = Time.time + 20;
        }
        
    }

    public GameObject RandomPrefab()
    {
		int random = UnityEngine.Random.Range(0, prefabIcon.Length);

	   return prefabIcon[random];

	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("Hello");
            GameOver();
        }

    }

    public void GameOver()
    {
        SceneManager.LoadScene(0);
    }
}
