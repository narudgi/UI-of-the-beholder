﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Character", menuName = "Character")]
public class StateCharacter : ScriptableObject
{
    [SerializeField]
    int maxHealth = 4;
    [SerializeField]
    int friction = 1;
    [SerializeField]
    int speciality = 0;

    public Sprite sprite;
    //public  abilityscript;

    bool deaded;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    { 
        
    }
}
